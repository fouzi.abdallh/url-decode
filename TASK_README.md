# WAF Interview Task

The task includes the following sub-tasks:
1.  Answer the [coding question below](#url-decoding-question)
2.  Add more tests, try to look for edge cases.
3.  Improve the tests code as described in [Improving tests](#improving-tests-framework) section.
4.  [Add a memory leak check](##memory-leak-check) for the test stages this should 
be done in a separate pipeline job.
5.  Write a short document (in Markdown, Org or txt) that describes your solution 
and analyze time and space complexity in Big-O notation.
This document should be commited to this repository as well.

Please note that this task will be checked on GitLab so
before you start coding and completing the subtasks, create a **public** git
repository at [GitLab.com](https://gitlab.com).

If you have some knowledge in git this is a good time to show us by using Merge
Requests (like GitHub Pull Requests), by writing informal commits and using
git best practices.
If you're unfamiliar with git this is also fine, we want you to try an experiment
with it and use it in this project.

Good luck.

## URL decoding question

Please write a function that does URL decoding

**Input**: string and length

**Output**: string that doesn't include a `"%"` that is followed by two
hexadecimal digits.

The function should scan the input string from start towards the end, 
and replace each % followed by two hexadecimal digits with the ASCII 
character that is represented by the value of the hex digits (0x00-0xFF which is 0-255).

**Examples**:

`%2f` will turn into `/`

`ab%2fcd` will turn into `ab/cd`

`%25` will turn into %

`xx%25yy` will turn into `xx%yy`

**Multiple decoding examples:**

`ab/cd` can be written as `ab%2fcd` (single decoding as the above example)

But can be also written as:

`ab%%32fcd` (two decoding)

`ab%2%65cd` (two decoding)

`ab%25%32%65cd` (two decoding)

Notes

1.  The output string does not contain the sequence
    `/%[0-9a-f][0-9a-f]/`. Instead of such sequence a single byte
    should appear.

2.  The algorithms should be a single pass over the input (O(n)) .
    However, you may look ahead or look back. You may not re-run on the
    complete string (no memove, memcpy of the whole string).

3.  Every character can appear in the input string.

4.  The output string must be a complete concurrent string (without
    blanks or markups).

5.  You may alter the input string (write over it), allocate more memory
    but of course, the less allocated memory the better.

6.  You have a helper function as follows:

`bool is_decodeable(char* p)` -- returns whether the sequence at p is a
decodable 3 characters.

`char decode(char* p)` returns the character that is decoded by the
sequence pointed by p.

## Improving tests framework
The tests are run using the script `tests/run-tests.sh`.  
In order to see what went wrong when a test failed, please add a diff between
expected and actual tests results.  

for example, right now on failure we see the following when a test fails:
```bash
Test no.2:
  Input: abc%33def Len: 9
  Output: abc%33def Len:9
  Test result: failed
```

Please change it so it will look like this:
```bash
Test no.2:
  Input: abc%33def Len: 9
  Output: abc%33def Len:9
  Diff:
       -abc3def
       +abc%33def
  Test result: failed
```

The diff output should be colored (`-abc3def` should be red and `+abc%33def` should
be green).

This change should work on pipeline and should only use `diff` or `git-diff` utilities,
don't create a script or a program that does this extenally.

## Memory leak check
In order to check if this program has memeory leaks please add a pipeline job
that does this check.  You can choose any memory leak checker you want for example
Valgrind.  

The job should be in the test stage, like so:
```
build -- test
       |
       - test-leaks
```

Please try to preserve the convention used in this pipeline and use the Makefile.  
This test should work on local linux machine as well.
