#!/bin/bash

set -e

TESTS_DIR="tests"
TESTS_FILE="tests.txt"
EXEC_LOC="./$2"
TEST_TO_RUN="$1"

if [[ ! -e ${TESTS_DIR} ]]; then
    echo "Error: please run this script from the project root."
    exit 1
fi

TESTS_PATH="${TESTS_DIR}/${TESTS_FILE}"
FAIL_SUITE=0
NUM_TEST=1

# Run tests
functional_test() {
    while L= read -r test; do
        test_arg=$(echo ${test} | cut -d ',' -f 1)
        expected_res=$(echo ${test} | cut -d ',' -f 2)
        expected_decode_res=$expected_res
        expected_res="${#expected_res},${expected_res}"
        actual_res=$(${EXEC_LOC} ${test_arg})
        result="success"
        if [[ ${actual_res} != ${expected_res} ]]; then
            result="failed"
            FAIL_SUITE=1
        fi

        res_len=$(echo ${actual_res} | cut -d "," -f 1)
        res_str=$(echo ${actual_res} | cut -d "," -f 2)
        echo "Test no.${NUM_TEST}:"
        echo "  Input: ${test_arg} Len: ${#test_arg}"
        echo "  Output: ${res_str} Len:${res_len}"
        DIFF=`diff  -u --color=always  <(echo "$expected_decode_res" ) <(echo "$res_str") | tail -n2`
        if [[ ! -z ${DIFF} ]]; then
            echo "  Diff: "
            printf "       %s\n" $DIFF
        fi
        echo "  Test result: ${result}"
        echo ""

        NUM_TEST=$((NUM_TEST+1))
    done < ${TESTS_PATH}

    echo "Testing done."

    if [[ ${FAIL_SUITE} -eq 1 ]]; then
        exit 1
    fi
}

leaks_test() {
    tests/bin/valgrind --leak-check=full --error-exitcode=1 ${EXEC_LOC} asdf%33aw
    echo "Testing done."
    FAIL_SUITE=`echo $?`
    if [[ ${FAIL_SUITE} -eq 1 ]]; then
        echo "  Test result: failed"
        exit 1
    fi
    echo "  Test result: success"
}


case $TEST_TO_RUN in
    functional_test)
        functional_test
        ;;
    leaks_test)
        leaks_test
        ;;
esac