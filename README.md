# URL decoding description 

We loop over the URL and for each 3 characters we check if they are decodeable
1.  if yes we decode this 3 characters and set the outcome in the output string the go to the next 3.
2.  else we copy the first character of the 3 to the output string
and then check othe 2 character with the next character if it's decodeable
3.  while the output string lenght is larger than 2 we look at last 3 characters
and check if it's decoable the we decode it's an set the result in the output string.

for example:
```
**starting position**
URL: %2%3%35
ouput:

**after step 2**
URL: %2%3%35
ouput:%

**after step 2**
URL: %2%3%35
ouput:%2

**after step 2**
URL: %2%3%35
ouput:%2%

**after step 2**
URL: %2%3%35
ouput:%2%3

**after step 1**
URL: %2%3%35
ouput:%2%35

**after step 3**
URL: %2%3%35
ouput:%2%35 -> %25 -> %
```

## Analyze time and space complexity
N is the lenght of the URL
**space complexity** we allocte the output string with URL lenght so the complexity is O(N).

**time complexity** we loop inside a loop. the firs loop iterate over the URL once. the second loop
iterate over the output string, but because it decode part of the out string the next iteration first loop will not  
iterate over again so the code inside the secone loop will execute N times at max for the entire outer loop.
so the complexity is O(N).