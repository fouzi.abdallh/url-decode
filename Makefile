CC=gcc
DECODE_BIN=decode
DECODE_DEBUG_BIN=decode_debug
DECODE_DIR=$(shell pwd)

all: compile test test_leaks

.PHONY: test

test:
	tests/run-tests.sh functional_test $(DECODE_BIN)

test_leaks:
	tar -xf valgrind-3.15.0.tar.bz2
	cd valgrind-3.15.0; ./configure --prefix=$(DECODE_DIR)/tests; make; make install
	export VALGRIND_LIB=$(DECODE_DIR)/tests/lib/valgrind/; tests/run-tests.sh leaks_test $(DECODE_DEBUG_BIN)

compile: decode.c
	$(CC) decode.c -o $(DECODE_BIN)
	$(CC) -g decode.c -o $(DECODE_DEBUG_BIN)

clean:
	rm -rf $(DECODE_BIN)
	rm -rf $(DECODE_DEBUG_BIN)
