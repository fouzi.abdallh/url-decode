#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdbool.h>

const char lookup_unescape[256] = {
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1,
  -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, 117, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, 117, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

bool
is_hex (char c)
{
  if (((c >= '0') && (c <= '9')) ||
      ((c >= 'A') && (c <= 'F')) || ((c >= 'a') && (c <= 'f')))
    {
      return true;
    }

  return false;
}

bool
is_decodable (const char *str)
{
  if (str[0] != '%')
    return false;
  if (is_hex (str[1]) == false)
    return false;
  if (is_hex (str[2]) == false)
    return false;
  return true;
}

unsigned char
decode (const char *dst)
{
  return (unsigned char) ((lookup_unescape[(unsigned int) *(dst + 1)] << 4) +
			  (lookup_unescape[(unsigned int) *(dst + 2)] &
			   0xff));
}


char *
getUnescapedString (const char *inputStr, int len, int *ret_len)
{
  int k,i;
  char *outputStr = NULL;
  outputStr = (char *) calloc(sizeof(char), len + 1);
  if (!outputStr) {
    printf("Failed allocating memory exiting...");
    exit(1);
  }

  for(i=0, k=0; i < len; k++) {
    if(is_decodable(&inputStr[i])) {
      outputStr[k] = decode(&inputStr[i]);
      i += 3;
    } else {
      outputStr[k] = inputStr[i++];
    }

    while (k > 1 && is_decodable(&outputStr[k-2])) {
        outputStr[k - 2] = decode(&outputStr[k-2]);
        outputStr[k - 1] = 0;
        outputStr[k] = 0;
        k -= 2;
    }
  }
  outputStr[k] = '\0';
  *ret_len = k;
  return outputStr;
}


int
main (int argc, char *argv[])
{
  int ret_len;
  char *ret_str = getUnescapedString(argv[1], strlen(argv[1]), &ret_len);

  printf("%d,%s\n", ret_len, ret_str);
  free(ret_str);
}
